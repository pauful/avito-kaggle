__author__ = 'pauful'

import sys
import codecs
import csv
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
from sklearn import cross_validation
from nltk.corpus import stopwords
import string
import random as rnd
#from sklearn.linear_model import SGDClassifier

data = []
syms = []
classes = []

titols = []
textes = []

testData = []
itemId = []
itemIdTrain = []

def correctText(line):
    text = filter(lambda c: not c.isdigit(), line)
    text = text.translate(None, string.punctuation)
    return text.decode('utf-8')

def readFile():
    with open('avito_train4.tsv', 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')
        numItems = 0
        for row in avitoReader:
            numItems += 1

        csvfile.seek(0)
        rnd.seed(0)

        sampleIndexes = set(rnd.sample(range(numItems),9999))
        avitoReader = csv.reader(csvfile, delimiter='\t')
        for row in avitoReader:

            if avitoReader.line_num in sampleIndexes:
                #print avitoReader.line_num
                #text = row[3] + " " + row[4]
            #syms.append(unicode(text, errors='ignore'))
            #text = filter(lambda c: not c.isdigit(), text)
            #textN = text.decode('utf-8')
                titols.append(correctText(row[3]))
                textes.append(correctText(row[4]))
            #print text
                classes.append(row[8])
                itemIdTrain.append(row[0])


def readFileTest():
    with open('avito_test.tsv', 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')
        for row in avitoReader:
            text = row[3] + " " + row[4]
            #print '####'
            #print text
            #syms.append(unicode(text, errors='ignore'))
            #text = text.decode('utf-8')
            testData.append(correctText(text))
            itemId.append(row[0])
            #print text

stopWords = stopwords.words('russian')
vectorizer = CountVectorizer(min_df=1, ngram_range=(1, 1))
transformer = TfidfTransformer()
logisticTit = LogisticRegression(C=1.0, penalty='l1')
logisticText = LogisticRegression(C=1.0, penalty='l1')

def getTfIdfTrain(train):
    #X = vectorizer.fit_transform(train)
    return vectorizer.fit_transform(train)
    #return transformer.fit_transform(X)

def getTfIdfTest(test):
    #X = vectorizer.transform(test)
    #return transformer.transform(X)
    return vectorizer.transform(test)


def train(tfidfTrain, labels):
    logisticTit.fit(tfidfTrain, labels)

def crossValidation(classifa, trainingInstances, labels):
    scores = cross_validation.cross_val_score(classifa, trainingInstances, labels, cv=5)
    print "Cross validation"
    print scores

def ownCrossvalidation(classifaTits, classifaText, trTits, trText, labels, totalItems):
    numTrainInstances = totalItems*70/100;

    classifaTits.fit(trTits[:numTrainInstances], labels[:numTrainInstances])
    classifaText.fit(trText[:numTrainInstances], labels[:numTrainInstances])

    totalBad = 0
    totalPositive = 0
    totalPositivePredicted = 0
    totalPositiveInTrain = 0
    for i in range(numTrainInstances+1, totalItems-1) :
        predictTit = classifaTits.predict(trTits[i])[0]
        predictTitProba = classifaTits.predict_proba(trTits[i])

        predictText = classifaText.predict(trText[i])[0]
        predictTextProba = classifaText.predict_proba(trText[i])

        pos = predictTitProba.T[1] * predictTextProba.T[1]
        neg = predictTitProba.T[0] * predictTextProba.T[0]

        posFinal = pos / (pos + neg)


        if((posFinal > 0.5 and labels[i] != '1') or (posFinal < 0.5 and labels[i] != '0') ) :
            totalBad += 1
            print str(posFinal) + " -> " + predictTit + " # " + predictText + " == " + labels[i] + " -> " + itemIdTrain[i]

        if posFinal > 0.5 :
            totalPositive += 1
        if(posFinal > 0.5 and labels[i] == '1'):
            totalPositivePredicted += 1
        if(labels[i] == '1') :
            totalPositiveInTrain += 1

    print "Bad from total = " + str(totalBad) + "/" + str(totalItems*30/100)
    print "Total positive = " + str(totalPositive) + "/" + str(totalItems*30/100)
    print "Total positive predicted = " + str(totalPositivePredicted) + "/" + str(totalItems*30/100)
    print "Total positive in total = " + str(totalPositiveInTrain)

def main():
    print "read file train"
    readFile()
    print "generate tf idf train"
    tfidftTitols = getTfIdfTrain(titols)
    tfidftTextes = getTfIdfTrain(textes)


    #test = [text]
    #print "read file test"
    #readFileTest()

    #print '>>>>>>>PAU'
    #print testData
    #print "generate tfidf test"
    #tfidftest = getTfIdfTest(testData)
    #print "start training"
    #train(tfidft,classes)

    #print tfidft.toarray()
    #print 'Prediction'
    #print logistic.classes_
    #print logistic.predict_proba(tfidftest)
    #print logistic.decision_function(tfidftest)
    #crossValidation(logistic, tfidft, classes)
    ownCrossvalidation(logisticTit, logisticText, tfidftTitols, tfidftTextes, classes, len(titols))

    #predicted_scores = logistic.predict_proba(tfidftest).T[1]
    #print 'write results'
    #f = open('results2.txt', "w")
    #f.write("id\n")

    #for pred_score, item_id in sorted(zip(predicted_scores, itemId), reverse = True):
    #    f.write("%s\n" % (item_id))
    #f.close()
    print 'DONE'

if __name__=="__main__":
    main()