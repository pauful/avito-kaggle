import sys
import codecs
import csv
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
from sklearn import cross_validation
from nltk.corpus import stopwords
import string
import random as rnd 

data = []
syms = []
classes = []

testData = []
itemId = []

def correctText(line):
    text = filter(lambda c: not c.isdigit(), line)
    text = text.translate(None, string.punctuation)
    return text.decode('utf-8')

def readFile():
    with open('avito_train.tsv', 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')
        numItems = 0
        for row in avitoReader:
            numItems += 1
        
        csvfile.seek(0)        
        rnd.seed(0)
        
        sampleIndexes = set(rnd.sample(range(numItems),20000))
        avitoReader = csv.reader(csvfile, delimiter='\t')
        for row in avitoReader:
            
            if avitoReader.line_num in sampleIndexes:
                #print avitoReader.line_num
                text = row[3] + " " + row[4]
            #syms.append(unicode(text, errors='ignore'))
            #text = filter(lambda c: not c.isdigit(), text)
            #textN = text.decode('utf-8')
                syms.append(correctText(text))
            #print text
                classes.append(row[8])
            

def readFileTest():
    with open('avito_test3.tsv', 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')
        for row in avitoReader:
            text = row[3] + " " + row[4]
            #print '####'
            #print text
            #syms.append(unicode(text, errors='ignore'))
            #text = text.decode('utf-8')
            testData.append(correctText(text))
            itemId.append(row[0])
            #print text
            
stopWords = stopwords.words('russian')
vectorizer = CountVectorizer(min_df=1, stop_words = stopWords)
transformer = TfidfTransformer()
logistic = LogisticRegression(C=1.0)

def getTfIdfTrain(train):
    X = vectorizer.fit_transform(train)
    return transformer.fit_transform(X)
    

def getTfIdfTest(test) :
    X = vectorizer.transform(test)
    return transformer.transform(X)


def train(tfidfTrain, labels):
    logistic.fit(tfidfTrain, labels)

def crossValidation(classifa, trainingInstances, labels):
    scores = cross_validation.cross_val_score(classifa, trainingInstances, labels, cv=5)
    print "Cross validation"
    print scores
    
def main():
    "read file train"
    readFile()
    print "generate tf idf train"
    tfidft = getTfIdfTrain(syms)
    
    
    #test = [text]
    print "read file test"
    readFileTest()
    
    #print '>>>>>>>PAU'
    #print testData
    print "generate tfidf test"
    tfidftest = getTfIdfTest(testData)
    print "start training"
    train(tfidft,classes)
    
    #print tfidft.toarray()
    print 'Prediction:'
    print logistic.classes_
    #print logistic.predict_proba(tfidftest)
    #print logistic.decision_function(tfidftest)
    crossValidation(logistic, tfidft, classes)
    
    predicted_scores = logistic.predict_proba(tfidftest).T[1]
    
    f = open('results.txt', "w")
    f.write("id\n")
    
    for pred_score, item_id in sorted(zip(predicted_scores, itemId), reverse = True):
        f.write("%s\n" % (item_id))
    f.close()
    

if __name__=="__main__":
    main()