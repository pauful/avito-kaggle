# coding: utf-8

import sys
import codecs
import csv
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk.stem.snowball import RussianStemmer
import numpy as np
from sklearn import cross_validation
from nltk.corpus import stopwords
import string
import random as rnd 
from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier


data = []
syms = []
classes = []
titles = []
testData = []
itemId = []
itemIdTrain = []
realPositive = []

def correctText(line):
    text = filter(lambda c: not c.isdigit(), line)
    text = text.translate(None, string.punctuation)
    text = text.decode('utf-8')
    newText = ''
    #print "Initial Text: " + text
    for w in text.split(' '):
        #print w
        #print stemmer.stem(w)
        newText += " " + stemmer.stem(w)
    #print "Final text: " + newText
    return newText;
    #return stemmer.stem(text)

def readFile():
    with open('avito_train.tsv', 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')
        numItems = 0
        for row in avitoReader:
            numItems += 1
        
        csvfile.seek(0)        
        rnd.seed(0)
        
        sampleIndexes = set(rnd.sample(range(numItems),2000000))
        avitoReader = csv.reader(csvfile, delimiter='\t')
        for row in avitoReader:
            
            if avitoReader.line_num in sampleIndexes:
                #print avitoReader.line_num
                text = row[3] + " " + row[4] + " " + row[5]
            #syms.append(unicode(text, errors='ignore'))
            #text = filter(lambda c: not c.isdigit(), text)
            #textN = text.decode('utf-8')
                syms.append(correctText(text))
                titles.append(row[1])
            #print text
                if row[7] == '1' and row[8] == '1':
                    classes.append(1)
                else:
                    classes.append(1)
                realPositive.append(row[8])
                #classes.append(row[8])

                itemIdTrain.append(row[0])
            

def readFileTest():
    with open('avito_test.tsv', 'rb') as csvfile:
        avitoReader = csv.reader(csvfile, delimiter='\t')
        for row in avitoReader:
            text = row[3] + " " + row[4] + " " + row[5]
            #print '####'
            #print text
            #syms.append(unicode(text, errors='ignore'))
            #text = text.decode('utf-8')
            testData.append(correctText(text))
            itemId.append(row[0])
            #print text

stemmer = RussianStemmer(ignore_stopwords=True)
#stopwordsUni = [word.decode('utf-8') for word in stopwords.words('russian')]
stopwordsUni= frozenset(word.decode('utf-8') for word in stopwords.words("russian") if word!="не")
vectorizer = CountVectorizer(min_df=1, ngram_range=(1, 2), stop_words=stopwordsUni)
transformer = TfidfTransformer()
logistic = LogisticRegression(C=1.0, penalty='l2')
#logistic = SGDClassifier(penalty="l1", class_weight={'1':1})


def getTfIdfTrain(train):
    #return vectorizer.fit_transform(train)
    X = vectorizer.fit_transform(train)
    return transformer.fit_transform(X)

def getTfIdfTest(test):
    X = vectorizer.transform(test)
    return transformer.transform(X)
    #return vectorizer.transform(test)


def train(tfidfTrain, labels):
    logistic.fit(tfidfTrain, labels)

def crossValidation(classifa, trainingInstances, labels):
    scores = cross_validation.cross_val_score(classifa, trainingInstances, labels, cv=5)
    print "Cross validation"
    print scores

def ownCrossvalidation(classifa, trainingInstances, labels, totalItems):
    numTrainInstances = totalItems*70/100;
    
    classifa.fit(trainingInstances[:numTrainInstances], realPositive[:numTrainInstances])#, sample_weight=labels[:numTrainInstances])
    
    totalBad = 0
    totalPositive = 0
    totalPositivePredicted = 0
    titleNum = {}
    for i in range(numTrainInstances+1, totalItems-1) :
        pred = classifa.predict(trainingInstances[i])[0]
        if(pred  !=  realPositive[i]) :
            totalBad += 1
            #print pred + " == " + realPositive[i] + " =>  " + titles[i]
            if realPositive[i] == '0':
                print itemIdTrain[i]
            if titles[i] not in titleNum :
                titleNum[titles[i]] = 0
            titleNum[titles[i]] += 1
            #print logistic.predict(trainingInstances[i])[0] + " == " + labels[i] + " -> " + itemIdTrain[i]
        if realPositive[i] == '1' :
            totalPositive += 1
        if(pred == realPositive[i] and realPositive[i] == '1'):
            totalPositivePredicted += 1
    print len(titleNum)
    print titleNum
    print 'Num words ' + str(trainingInstances.getnnz())
    print "Bad from total = " + str(totalBad) + "/" + str(totalItems*30/100)
    print "Total positive = " + str(totalPositive) + "/" + str(totalItems*30/100)
    print "Total positive predicted = " + str(totalPositivePredicted) + "/" + str(totalItems*30/100)
        
def main():
    print "read file train"
    readFile()
    print "generate tf idf train"
    tfidft = getTfIdfTrain(syms)
    
    
    #test = [text]
    print "read file test"
    readFileTest()
    
    #print '>>>>>>>PAU'
    #print testData
    print "generate tfidf test"
    tfidftest = getTfIdfTest(testData)
    #print "start training"
    train(tfidft,realPositive)
    
    #print tfidft.toarray()
    print 'Prediction'
    #print logistic.classes_
    #print logistic.predict_proba(tfidftest)
    #print logistic.decision_function(tfidftest)
    #crossValidation(logistic, tfidft, classes)
    #ownCrossvalidation(logistic, tfidft, classes, len(syms))
    
    predicted_scores = logistic.predict_proba(tfidftest).T[1]
    #print logistic.decision_function(tfidftest)
    #predicted_scores = logistic.predict(tfidftest)
    #for a in predicted_scores :
    #    print a
    #print 'write results'
    f = open('results6.txt', "w")
    f.write("id\n")
    
    for pred_score, item_id in sorted(zip(predicted_scores, itemId), reverse = True):
    #for pred_score, item_id in zip(predicted_scores, itemId):
        f.write("%s\n" % (item_id))
    f.close()
    print 'DONE'

if __name__=="__main__":
    main()